#include "core/Assert.hpp"
#include "core/Event.hpp"

namespace Real {

EventBase::EventBase(Category category) noexcept : category(category) {}

bool EventBase::is_in_category(Category category) const noexcept
{
    return static_cast<unsigned>(this->category) &
           static_cast<unsigned>(category);
}

std::string EventBase::get_information() const noexcept
{
    return std::string(get_name());
}

std::ostream& operator<<(std::ostream& os, const EventBase& event)
{
    return os << event.get_information();
}

EventBase::Category operator|(EventBase::Category first,
                              EventBase::Category second) noexcept
{
    return static_cast<EventBase::Category>(static_cast<unsigned>(first) |
                                            static_cast<unsigned>(second));
}

int KeyEvent::get_keycode() const noexcept
{
    return keycode;
}

KeyEvent::KeyEvent(int keycode) noexcept
    : EventBase(Category::Keyboard | Category::Input), keycode(keycode)
{}

KeyPressedEvent::KeyPressedEvent(int keycode, int repeat_count) noexcept
    : KeyEvent(keycode), repeat_count(repeat_count)
{}

unsigned KeyPressedEvent::get_repeat_count() const noexcept
{
    return repeat_count;
}

std::string_view KeyPressedEvent::get_name() const noexcept
{
    return "KeyPressed";
}

std::string KeyPressedEvent::get_information() const noexcept
{
    return fmt::format(
        "{}: {} ({} repeats)", get_name(), get_keycode(), repeat_count);
}

KeyReleasedEvent::KeyReleasedEvent(int keycode) noexcept : KeyEvent(keycode) {}

std::string_view KeyReleasedEvent::get_name() const noexcept
{
    return "KeyReleased";
}

std::string KeyReleasedEvent::get_information() const noexcept
{
    return fmt::format("{}: {}", get_name(), get_keycode());
}

KeyTypedEvent::KeyTypedEvent(int keycode) noexcept : KeyEvent(keycode) {}

std::string_view KeyTypedEvent::get_name() const noexcept
{
    return "KeyTyped";
}

std::string KeyTypedEvent::get_information() const noexcept
{
    return fmt::format("{}: {}", get_name(), get_keycode());
}

MouseMovedEvent::MouseMovedEvent(double x, double y) noexcept
    : EventBase(Category::Mouse | Category::Input), x(x), y(y)
{}

double MouseMovedEvent::get_x() const noexcept
{
    return x;
}

double MouseMovedEvent::get_y() const noexcept
{
    return y;
}

std::string_view MouseMovedEvent::get_name() const noexcept
{
    return "MouseMoved";
}

std::string MouseMovedEvent::get_information() const noexcept
{
    return fmt::format("{}: {}, {}", get_name(), x, y);
}

MouseScrolledEvent::MouseScrolledEvent(double x_offset,
                                       double y_offset) noexcept
    : EventBase(Category::Mouse | Category::Input),
      x_offset(x_offset),
      y_offset(y_offset)
{}

double MouseScrolledEvent::get_x_offset() const noexcept
{
    return x_offset;
}

double MouseScrolledEvent::get_y_offset() const noexcept
{
    return y_offset;
}

std::string_view MouseScrolledEvent::get_name() const noexcept
{
    return "MouseScrolled";
}

std::string MouseScrolledEvent::get_information() const noexcept
{
    return fmt::format("{}: {}, {}", get_name(), x_offset, y_offset);
}

int MouseButtonEvent::get_button() const noexcept
{
    return button;
}

MouseButtonEvent::MouseButtonEvent(int button) noexcept
    : EventBase(Category::Mouse | Category::Input), button(button)
{}

MouseButtonPressedEvent::MouseButtonPressedEvent(int button) noexcept
    : MouseButtonEvent(button)
{}

std::string_view MouseButtonPressedEvent::get_name() const noexcept
{
    return "MouseButtonPressed";
}

std::string MouseButtonPressedEvent::get_information() const noexcept
{
    return fmt::format("{}: {}", get_name(), get_button());
}

MouseButtonReleasedEvent::MouseButtonReleasedEvent(int button) noexcept
    : MouseButtonEvent(button)
{}

std::string_view MouseButtonReleasedEvent::get_name() const noexcept
{
    return "MouseButtonReleased";
}

std::string MouseButtonReleasedEvent::get_information() const noexcept
{
    return fmt::format("{}: {}", get_name(), get_button());
}

WindowResizeEvent::WindowResizeEvent(unsigned width, unsigned height) noexcept
    : EventBase(Category::Application), width(width), height(height)
{}

unsigned WindowResizeEvent::get_width() const noexcept
{
    return width;
}

unsigned WindowResizeEvent::get_height() const noexcept
{
    return height;
}

std::string_view WindowResizeEvent::get_name() const noexcept
{
    return "WindowResize";
}

std::string WindowResizeEvent::get_information() const noexcept
{
    return fmt::format("{}: {}, {}", get_name(), width, height);
}

WindowCloseEvent::WindowCloseEvent() noexcept : EventBase(Category::Application)
{}

std::string_view WindowCloseEvent::get_name() const noexcept
{
    return "WindowClose";
}

AppTickEvent::AppTickEvent() noexcept : EventBase(Category::Application) {}

std::string_view AppTickEvent::get_name() const noexcept
{
    return "AppTick";
}

AppUpdateEvent::AppUpdateEvent() noexcept : EventBase(Category::Application) {}

std::string_view AppUpdateEvent::get_name() const noexcept
{
    return "AppUpdate";
}

AppRenderEvent::AppRenderEvent() noexcept : EventBase(Category::Application) {}

std::string_view AppRenderEvent::get_name() const noexcept
{
    return "AppRender";
}

bool is_handled(const Event& event) noexcept
{
    return std::visit(
        [](auto&& event) noexcept -> bool {
            using T = std::decay_t<decltype(event)>;
            static_assert(
                is_public_base<EventBase, T>,
                "EventBase must be a public base class of every event type.");
            return event.handled;
        },
        event);
}

}
