#include "core/Log.hpp"

namespace Real {

std::string Log::Core::reformat(std::string_view format_string)
{
    return fmt::format("[ENGINE] {}\n", format_string);
}

std::string Log::reformat(std::string_view format_string)
{
    return fmt::format("{}\n", format_string);
}

}
