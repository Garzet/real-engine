#include "core/Window.hpp"

#include "platform/linux/LinuxWindow.hpp"

namespace Real {

WindowProperties::WindowProperties(std::string title,
                                   unsigned width,
                                   unsigned height)
    : title(std::move(title)), width(width), height(height)
{}

std::unique_ptr<Window> Window::create(const WindowProperties& props)
{
    return std::make_unique<LinuxWindow>(props);
}

}
