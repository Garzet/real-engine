#include "core/Timestep.hpp"

namespace Real {

Timestep::Timestep(Duration time) noexcept : time(time) {}

std::chrono::seconds Timestep::get_seconds() const noexcept
{
    return std::chrono::duration_cast<std::chrono::seconds>(time);
}

std::chrono::milliseconds Timestep::get_milliseconds() const noexcept
{
    return std::chrono::duration_cast<std::chrono::milliseconds>(time);
}

std::chrono::microseconds Timestep::get_microseconds() const noexcept
{
    return std::chrono::duration_cast<std::chrono::microseconds>(time);
}

std::chrono::nanoseconds Timestep::get_nanoseconds() const noexcept
{
    return std::chrono::duration_cast<std::chrono::nanoseconds>(time);
}

Timestep::operator float() const noexcept
{
    constexpr auto nanoseconds_per_second = 1e9f;
    return get_nanoseconds().count() / nanoseconds_per_second;
}

Timepoint Timestep::get_current_timepoint() noexcept
{
    return TimestepClock::now();
}

std::ostream& operator<<(std::ostream& s, const Timestep& timestep)
{
    if (timestep.time > std::chrono::milliseconds(10)) {
        s << timestep.get_milliseconds().count() << "ms";
    } else {
        s << timestep.get_microseconds().count() << "us";
    }
    return s;
}

}
