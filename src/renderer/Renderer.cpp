#include "renderer/Renderer.hpp"

#include "renderer/Renderer2D.hpp"

#include <GL/glew.h>

namespace Real {

std::unique_ptr<Renderer::SceneData> Renderer::scene_data =
    std::make_unique<Renderer::SceneData>();

void Renderer::init()
{
    RenderCommand::init();
    Renderer2D::init();

    glEnable(GL_DEPTH_TEST);
}

void Renderer::shutdown()
{
    Renderer2D::shutdown();
}

void Renderer::on_window_resize(unsigned width, unsigned height)
{
    RenderCommand::set_viewport(0, 0, width, height);
}

void Renderer::begin_scene(OrthographicCamera& camera)
{
    scene_data->view_projection_matrix = camera.get_view_projection_matrix();
}

void Renderer::end_scene() {}

void Renderer::submit(const std::shared_ptr<Shader>& shader,
                      const std::shared_ptr<VertexArray>& vertex_array,
                      const glm::mat4& transform)
{
    shader->bind();
    shader->set_uniform_mat4("view_projection",
                             scene_data->view_projection_matrix);
    shader->set_uniform_mat4("transform", transform);

    vertex_array->bind();
    RenderCommand::draw_indexed(*vertex_array);
}

RendererAPI::API Renderer::get_api() noexcept
{
    return RendererAPI::get_api();
}

}
