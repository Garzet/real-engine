#include "renderer/Texture.hpp"

#include "core/Assert.hpp"
#include "renderer/Renderer.hpp"
#include "platform/opengl/OpenGLTexture.hpp"

namespace Real {

std::shared_ptr<Texture2D> Texture2D::create(const std::filesystem::path& filepath)
{
    switch (Renderer::get_api()) {
        case RendererAPI::API::OpenGL:
            return std::make_shared<OpenGLTexture2D>(filepath);
        default: REAL_ASSERT_UNREACHABLE("Unsupported render API.");
    }
}

}
