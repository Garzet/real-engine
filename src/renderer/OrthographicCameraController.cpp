#include "renderer/OrthographicCameraController.hpp"

#include "core/Input.hpp"
#include "core/Key.hpp"
#include "core/Core.hpp"

namespace Real {

OrthographicCameraController::OrthographicCameraController(
    float aspect_ratio, bool rotation) noexcept
    : aspect_ratio(aspect_ratio),
      camera(-aspect_ratio * zoom_level,
             aspect_ratio * zoom_level,
             -zoom_level,
             zoom_level),
      rotation(rotation)
{}

void OrthographicCameraController::on_update(Timestep timestep) noexcept
{
    // Horizontal movement keys.
    if (Input::is_key_pressed(Key::Left) || Input::is_key_pressed(Key::H) ||
        Input::is_key_pressed(Key::A)) {
        camera_position.x -= camera_translation_speed * timestep;
    }
    if (Input::is_key_pressed(Key::Right) || Input::is_key_pressed(Key::L) ||
        Input::is_key_pressed(Key::D)) {
        camera_position.x += camera_translation_speed * timestep;
    }

    // Vertical movement keys.
    if (Input::is_key_pressed(Key::Up) || Input::is_key_pressed(Key::K) ||
        Input::is_key_pressed(Key::W)) {
        camera_position.y += camera_translation_speed * timestep;
    }
    if (Input::is_key_pressed(Key::Down) || Input::is_key_pressed(Key::J) ||
        Input::is_key_pressed(Key::S)) {
        camera_position.y -= camera_translation_speed * timestep;
    }

    if (rotation) { // Check if rotation is enabled.
        // Rotation movement keys.
        if (Input::is_key_pressed(Key::Q)) {
            camera_rotation += camera_rotation_speed * timestep;
        }
        if (Input::is_key_pressed(Key::E)) {
            camera_rotation -= camera_rotation_speed * timestep;
        }

        camera.set_rotation(camera_rotation);
    }

    camera.set_position(camera_position);
}

void OrthographicCameraController::on_event(Event& event) noexcept
{
    std::visit(
        [this](auto&& event) {
            using T = std::decay_t<decltype(event)>;
            if constexpr (std::is_same_v<MouseScrolledEvent, T>) {
                on_mouse_scroll(event);
            } else if constexpr (std::is_same_v<MouseScrolledEvent, T>) {
                on_window_resize(event);
            }
        },
        event);
}

void OrthographicCameraController::on_mouse_scroll(
    MouseScrolledEvent& event) noexcept
{
    zoom_level -= event.get_y_offset() * 0.25f; // Slow down zoom.
    zoom_level = std::max(zoom_level, 0.25f);   // Do not allow infinite zoom.
    camera.set_projection(-aspect_ratio * zoom_level,
                          aspect_ratio * zoom_level,
                          -zoom_level,
                          zoom_level);
}

void OrthographicCameraController::on_window_resize(
    WindowResizeEvent& event) noexcept
{
    aspect_ratio = static_cast<float>(event.get_width()) / event.get_height();
    camera.set_projection(-aspect_ratio * zoom_level,
                          aspect_ratio * zoom_level,
                          -zoom_level,
                          zoom_level);
}

}
