#include "renderer/Shader.hpp"

#include "core/Assert.hpp"
#include "renderer/Renderer.hpp"
#include "platform/opengl/OpenGLShader.hpp"

namespace Real {

std::shared_ptr<Shader> Shader::create(const std::filesystem::path& filepath)
{
    switch (Renderer::get_api()) {
        case RendererAPI::API::OpenGL:
            return std::make_shared<OpenGLShader>(filepath);
        default: REAL_ASSERT_UNREACHABLE("Unsupported render API.");
    }
}

std::shared_ptr<Shader> Shader::create(const std::string& name,
                           const std::string& vertex_src,
                           const std::string& fragment_src)
{
    switch (Renderer::get_api()) {
        case RendererAPI::API::OpenGL:
            return std::make_shared<OpenGLShader>(
                name, vertex_src, fragment_src);
        default: REAL_ASSERT_UNREACHABLE("Unsupported render API.");
    }
}

void ShaderLibrary::add(const std::string& name, const std::shared_ptr<Shader>& shader)
{
    REAL_ASSERT_FMT(
        !exists(name), "Shader '{}' already exists in the library.", name);
    shaders[name] = shader;
}

void ShaderLibrary::add(const std::shared_ptr<Shader>& shader)
{
    add(shader->get_name(), shader);
}

std::shared_ptr<Shader> ShaderLibrary::load(const std::filesystem::path& filepath)
{
    auto shader = Shader::create(filepath);
    add(shader->get_name(), shader);
    return shader;
}

std::shared_ptr<Shader> ShaderLibrary::load(const std::string& name,
                                const std::filesystem::path& filepath)
{
    auto shader = Shader::create(filepath);
    add(name, shader);
    return shader;
}

std::shared_ptr<Shader> ShaderLibrary::get(const std::string& name)
{
    REAL_ASSERT_FMT(exists(name), "Shader '{}' not in the library.", name);
    return shaders[name];
}

bool ShaderLibrary::exists(const std::string& name) const
{
    return shaders.find(name) != shaders.end();
}

}
