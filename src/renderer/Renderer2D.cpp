#include "renderer/Renderer2D.hpp"

#include "renderer/VertexArray.hpp"
#include "renderer/Shader.hpp"
#include "renderer/RenderCommand.hpp"

#include <glm/gtc/matrix_transform.hpp>

namespace Real {

namespace {

struct Renderer2DStorage {
    std::shared_ptr<VertexArray> vertex_array;
    std::shared_ptr<Shader> flat_color_shader;
    std::shared_ptr<Shader> texture_shader;
};

std::unique_ptr<Renderer2DStorage> renderer_data;

}

void Renderer2D::init()
{
    renderer_data                = std::make_unique<Renderer2DStorage>();
    renderer_data->vertex_array  = VertexArray::create();
    float square_vertices[5 * 4] = {-0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 0.5f, -0.5f,
                                    0.0f,  1.0f,  0.0f, 0.5f, 0.5f, 0.0f, 1.0f,
                                    1.0f,  -0.5f, 0.5f, 0.0f, 0.0f, 1.0f};
    auto vertex_buffer           = VertexBuffer::create(
        square_vertices, sizeof(square_vertices) / sizeof(float));
    vertex_buffer->set_layout({{ShaderDataType::Float3, "position"},
                               {ShaderDataType::Float2, "tex_coord"}});
    renderer_data->vertex_array->add_vertex_buffer(vertex_buffer);

    unsigned square_indices[6] = {0, 1, 2, 2, 3, 0};
    auto square_ib             = IndexBuffer::create(square_indices, 6);
    renderer_data->vertex_array->set_index_buffer(square_ib);

    renderer_data->flat_color_shader =
        Shader::create("res/shaders/FlatColor.glsl");

    renderer_data->texture_shader = Shader::create("res/shaders/Texture.glsl");
    renderer_data->texture_shader->bind();
    renderer_data->texture_shader->set_uniform_int("texture_sampler", 0);
}

void Renderer2D::shutdown()
{
    renderer_data.reset(); // Free renderer data memory.
}

void Renderer2D::begin_scene(const OrthographicCamera& camera)
{
    renderer_data->flat_color_shader->bind();
    renderer_data->flat_color_shader->set_uniform_mat4(
        "view_projection", camera.get_view_projection_matrix());

    renderer_data->texture_shader->bind();
    renderer_data->texture_shader->set_uniform_mat4(
        "view_projection", camera.get_view_projection_matrix());
}

void Renderer2D::end_scene() {}

void Renderer2D::draw_quad(const glm::vec2& position,
                           const glm::vec2& size,
                           const glm::vec4& color)
{
    draw_quad({position.x, position.y, 0.0f}, size, color);
}

void Renderer2D::draw_quad(const glm::vec3& position,
                           const glm::vec2& size,
                           const glm::vec4& color)
{
    renderer_data->flat_color_shader->bind();
    renderer_data->flat_color_shader->set_uniform_float4("uniform_color",
                                                         color);

    // Calculate transform and upload it.
    glm::mat4 transform = glm::translate(glm::mat4(1.0f), position) *
                          glm::scale(glm::mat4(1.0f), {size.x, size.y, 1.0f});
    renderer_data->flat_color_shader->set_uniform_mat4("transform", transform);

    renderer_data->vertex_array->bind();
    RenderCommand::draw_indexed(*renderer_data->vertex_array);
}

void Renderer2D::draw_quad(const glm::vec2& position,
                           const glm::vec2& size,
                           const Texture2D& texture)
{
    draw_quad({position.x, position.y, 0.0f}, size, texture);
}

void Renderer2D::draw_quad(const glm::vec3& position,
                           const glm::vec2& size,
                           const Texture2D& texture)
{
    renderer_data->texture_shader->bind();

    // Calculate transform and upload it.
    glm::mat4 transform = glm::translate(glm::mat4(1.0f), position) *
                          glm::scale(glm::mat4(1.0f), {size.x, size.y, 1.0f});
    renderer_data->texture_shader->set_uniform_mat4("transform", transform);

    texture.bind();

    renderer_data->vertex_array->bind();
    RenderCommand::draw_indexed(*renderer_data->vertex_array);
}

}
