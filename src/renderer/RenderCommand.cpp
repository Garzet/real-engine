#include "renderer/RenderCommand.hpp"

#include "platform/opengl/OpenGLRendererAPI.hpp"

namespace Real {

// Hardcoded renderer API. Could be set at runtime in the future.
std::unique_ptr<RendererAPI> RenderCommand::renderer_api =
    std::make_unique<OpenGLRendererAPI>();

void RenderCommand::init() noexcept
{
    renderer_api->init();
}

void RenderCommand::set_viewport(unsigned x,
                                 unsigned y,
                                 unsigned width,
                                 unsigned height) noexcept
{
    renderer_api->set_viewport(x, y, width, height);
}

void RenderCommand::set_clear_color(const glm::vec4& color) noexcept
{
    renderer_api->set_clear_color(color);
}

void RenderCommand::clear() noexcept
{
    renderer_api->clear();
}

void RenderCommand::draw_indexed(const VertexArray& vertex_array) noexcept
{
    renderer_api->draw_indexed(vertex_array);
}

}
