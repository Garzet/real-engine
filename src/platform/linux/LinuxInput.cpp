#include "platform/linux/LinuxInput.hpp"

#include "core/Application.hpp"
#include <GLFW/glfw3.h>

namespace Real {

/* Input instance singleton object pointer. Can be initialized statically since
 * the class has no member which need to be destructed (it is just a collection
 * of functions). */
std::unique_ptr<Input> Input::instance = std::make_unique<LinuxInput>();

bool LinuxInput::is_key_pressed_impl(Key key)
{
    // Get native window pointer.
    auto window = static_cast<GLFWwindow*>(
        Application::get().get_window().get_native_window());

    auto state = glfwGetKey(window, static_cast<int>(key)); // Get key state.
    return state == GLFW_PRESS || state == GLFW_REPEAT;
}

bool LinuxInput::is_mouse_button_pressed_impl(int button)
{
    // Get native window pointer.
    auto window = static_cast<GLFWwindow*>(
        Application::get().get_window().get_native_window());

    auto state = glfwGetMouseButton(window, button); // Get mouse button state.
    return state == GLFW_PRESS;
}

float LinuxInput::get_mouse_x_impl()
{
    auto [x, y] = get_mouse_position_impl();
    return x;
}

float LinuxInput::get_mouse_y_impl()
{
    auto [x, y] = get_mouse_position_impl();
    return y;
}

std::pair<float, float> LinuxInput::get_mouse_position_impl()
{
    // Get native window pointer.
    auto window = static_cast<GLFWwindow*>(
        Application::get().get_window().get_native_window());

    double x_pos, y_pos;
    glfwGetCursorPos(window, &x_pos, &y_pos); // Get mouse cursor position.
    return {static_cast<float>(x_pos), static_cast<float>(y_pos)};
}

}
