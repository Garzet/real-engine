#include "platform/opengl/OpenGLBuffer.hpp"

#include <GL/glew.h>

namespace Real {

OpenGLVertexBuffer::OpenGLVertexBuffer(float* vertices, unsigned count) noexcept
{
    glCreateBuffers(1, &renderer_id);
    glBindBuffer(GL_ARRAY_BUFFER, renderer_id);
    glBufferData(
        GL_ARRAY_BUFFER, count * sizeof(float), vertices, GL_STATIC_DRAW);
}

OpenGLVertexBuffer::~OpenGLVertexBuffer() noexcept
{
    if (renderer_id) { glDeleteBuffers(1, &renderer_id); }
}

OpenGLVertexBuffer::OpenGLVertexBuffer(OpenGLVertexBuffer&& other) noexcept
    : renderer_id(other.renderer_id), layout(std::move(other.layout))
{
    other.renderer_id = 0;
}

OpenGLVertexBuffer&
OpenGLVertexBuffer::operator=(OpenGLVertexBuffer&& other) noexcept
{
    if (renderer_id) { glDeleteBuffers(1, &renderer_id); }

    renderer_id = other.renderer_id;
    layout      = std::move(other.layout);

    other.renderer_id = 0;

    return *this;
}

void OpenGLVertexBuffer::set_layout(BufferLayout layout) noexcept
{
    this->layout = std::move(layout);
}

const BufferLayout& OpenGLVertexBuffer::get_layout() const noexcept
{
    return layout;
}

void OpenGLVertexBuffer::bind() const noexcept
{
    glBindBuffer(GL_ARRAY_BUFFER, renderer_id);
}

void OpenGLVertexBuffer::unbind() const noexcept
{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

OpenGLIndexBuffer::OpenGLIndexBuffer(unsigned* indices, unsigned count)
    : count(count)
{
    glCreateBuffers(1, &renderer_id);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, renderer_id);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 count * sizeof(unsigned),
                 indices,
                 GL_STATIC_DRAW);
}

OpenGLIndexBuffer::OpenGLIndexBuffer(OpenGLIndexBuffer&& other) noexcept
    : renderer_id(other.renderer_id), count(other.count)
{
    other.renderer_id = 0;
}

OpenGLIndexBuffer&
OpenGLIndexBuffer::operator=(OpenGLIndexBuffer&& other) noexcept
{
    if (renderer_id) { glDeleteBuffers(1, &renderer_id); }

    renderer_id = other.renderer_id;
    count       = other.count;

    other.renderer_id = 0;

    return *this;
}

OpenGLIndexBuffer::~OpenGLIndexBuffer() noexcept
{
    if (renderer_id) { glDeleteBuffers(1, &renderer_id); }
}

unsigned OpenGLIndexBuffer::get_count() const noexcept
{
    return count;
}

void OpenGLIndexBuffer::bind() const noexcept
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, renderer_id);
}

void OpenGLIndexBuffer::unbind() const noexcept
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

}
