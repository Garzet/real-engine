/* ----------------------------- VERTEX SHADER ------------------------------ */
#type vertex
#version 330 core

layout(location = 0) in vec3 position;

uniform mat4 view_projection;
uniform mat4 transform;

out vec3 varying_position;

void main()
{
    varying_position = position;
    gl_Position = view_projection
                     * transform * vec4(position, 1.0);
}
/* -------------------------------------------------------------------------- */


/* ---------------------------- FRAGMENT SHADER ----------------------------- */
#type fragment
#version 330 core

layout(location = 0) out vec4 color;

uniform vec4 uniform_color;

in vec3 varying_position;

void main()
{
    color = vec4(uniform_color);
}
/* -------------------------------------------------------------------------- */
