#ifndef SANDBOX_2D_HPP
#define SANDBOX_2D_HPP

#include <Real.hpp>

class Sandbox2D : public Real::Layer {
  private:
    Real::OrthographicCameraController camera_controller;
    //std::shared_ptr<Real::VertexArray> vertex_array;
    //std::shared_ptr<Real::Shader> shader;
    std::shared_ptr<Real::Texture2D> checkerboard;
    glm::vec4 square_color = {0.2f, 0.3f, 0.8f, 1.0f};

  public:
    Sandbox2D();
    ~Sandbox2D() override = default;

    void on_attach() override;
    void on_update(Real::Timestep timestep) override;
    void on_imgui_render() override;
    void on_event(Real::Event& event) override;
};

#endif
