#ifndef REAL_PRECOMPILED_HEADER_HPP
#define REAL_PRECOMPILED_HEADER_HPP

#include <type_traits>

#include <iostream>
#include <memory>
#include <utility>
#include <algorithm>
#include <functional>

#include <string>
#include <filesystem>
#include <sstream>
#include <vector>
#include <array>
#include <variant>
#include <unordered_map>
#include <unordered_set>

#include "core/Log.hpp"
#include "core/Assert.hpp"
#include "core/Application.hpp"
#include "core/Event.hpp"
#include "core/Input.hpp"
#include "core/Key.hpp"
#include "core/Layer.hpp"
#include "core/LayerStack.hpp"
#include "core/MouseButton.hpp"
#include "core/Timestep.hpp"
#include "core/Window.hpp"

#endif
