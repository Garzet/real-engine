#ifndef REAL_BUFFER_HPP
#define REAL_BUFFER_HPP

#include <memory>
#include <vector>

#include "core/Core.hpp"

namespace Real {

/** Defines supported shader data types. */
enum class ShaderDataType {
    None,
    Float,
    Float2,
    Float3,
    Float4,
    Mat3,
    Mat4,
    Int,
    Int2,
    Int3,
    Int4,
    Bool
};

/** Determines the size in bytes of a provided supported shader data type. */
unsigned shader_data_type_size(ShaderDataType type) noexcept;

/** Abstraction of a single element (e.g. position, texture coordinates, etc.)
 * in a vertex buffer. In OpenGL, this is referred to as attribute. */
class BufferElement {
  public:
    std::string name    = "";
    ShaderDataType type = ShaderDataType::None;
    unsigned size       = 0;
    unsigned offset     = 0;
    bool normalized     = false;

  public:
    BufferElement() = default;
    BufferElement(ShaderDataType type,
                  std::string name,
                  bool normalized = false) noexcept;

    unsigned get_component_count() const noexcept;
};

class BufferLayout {
  private:
    std::vector<BufferElement> elements;
    unsigned stride = 0;

  public:
    BufferLayout() = default;
    BufferLayout(std::initializer_list<BufferElement> elements);

    const std::vector<BufferElement>& get_elements() const noexcept;
    unsigned get_stride() const noexcept;

    std::vector<BufferElement>::iterator begin() noexcept;
    std::vector<BufferElement>::iterator end() noexcept;
    std::vector<BufferElement>::const_iterator begin() const noexcept;
    std::vector<BufferElement>::const_iterator end() const noexcept;

  private:
    void calculate_offsets_and_stride() noexcept;
};

/** Rendering API agnostic vertex buffer representation. */
class VertexBuffer {
  public:
    virtual ~VertexBuffer() = default;

    virtual const BufferLayout& get_layout() const = 0;
    virtual void set_layout(BufferLayout layout)   = 0;

    virtual void bind() const   = 0;
    virtual void unbind() const = 0;

    static std::shared_ptr<VertexBuffer> create(float* vertices,
                                                unsigned count);
};

/** Rendering API agnostic index buffer representation. */
class IndexBuffer {
  public:
    virtual ~IndexBuffer() = default;

    virtual unsigned get_count() const = 0;

    virtual void bind() const   = 0;
    virtual void unbind() const = 0;

    static std::shared_ptr<IndexBuffer> create(unsigned* indices,
                                               unsigned count);
};

}

#endif
