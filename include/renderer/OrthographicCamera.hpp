#ifndef REAL_ORTHOGRAPHIC_CAMERA_HPP
#define REAL_ORTHOGRAPHIC_CAMERA_HPP

#include <glm/glm.hpp>

namespace Real {

/* Orthographic camera used for 2D rendering. */
class OrthographicCamera {
  private:
    glm::mat4 projection_matrix;
    glm::mat4 view_matrix;

    /* Cache of the view-projection matrix to avoid excess multiplication. */
    glm::mat4 view_projection_matrix;

    glm::vec3 position = {0.0f, 0.0f, 0.0f}; // Position of the camera.
    float rotation     = 0.0f;               // Rotation of the camera.
  public:
    OrthographicCamera(float left,
                       float right,
                       float bottom,
                       float top) noexcept;

    void
    set_projection(float left, float right, float bottom, float top) noexcept;

    /** Sets the camera position. View matrix is recalculated. */
    void set_position(const glm::vec3& position) noexcept;

    /** Sets the camera rotation. View matrix is recalculated. */
    void set_rotation(float rotation) noexcept;

    const glm::vec3& get_position() const noexcept;
    float get_rotation() const noexcept;
    const glm::mat4& get_projection_matrix() const noexcept;
    const glm::mat4& get_view_matrix() const noexcept;
    const glm::mat4& get_view_projection_matrix() const noexcept;

  private:
    /* Updates the view matrix based on the class properties. This is invoked
     * whenever camera position or camera rotation is set. */
    void recalculate_view_matrix() noexcept;
};

}

#endif
