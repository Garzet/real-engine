#ifndef REAL_RENDERER_HPP
#define REAL_RENDERER_HPP

#include "renderer/RenderCommand.hpp"
#include "renderer/RendererAPI.hpp"
#include "renderer/OrthographicCamera.hpp"
#include "renderer/Shader.hpp"

namespace Real {

/** High level rendering construct that deals with high level concepts (meshes,
 * environments, scenes, lighting). It uses render commands to achieve those
 * concepts. */
class Renderer {
    // ------ TODO: Remove scene data to make renderer completely static -------
  private:
    struct SceneData {
        glm::mat4 view_projection_matrix;
    };
    static std::unique_ptr<SceneData> scene_data;
    // -------------------------------------------------------------------------

  public:
    static void init();
    static void shutdown();

    static void on_window_resize(unsigned width, unsigned height);

    static void begin_scene(OrthographicCamera& camera);
    static void end_scene();

    static void submit(const std::shared_ptr<Shader>& shader,
                       const std::shared_ptr<VertexArray>& vertex_array,
                       const glm::mat4& transform = glm::mat4(1.0f));

    static RendererAPI::API get_api() noexcept;
};

}

#endif
