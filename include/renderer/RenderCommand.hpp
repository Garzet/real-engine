#ifndef REAL_RENDER_COMMAND_HPP
#define REAL_RENDER_COMMAND_HPP

#include "RendererAPI.hpp"

namespace Real {

/** Provides static access to rendering API agnostic commands. Note that render
 * commands are supposed to do one thing and do one thing only - any other
 * commands (for example, binding) should be done before invoking a render
 * command. */
class RenderCommand {
  private:
    static std::unique_ptr<RendererAPI> renderer_api;

  public:
    static void init() noexcept;

    static void set_viewport(unsigned x,
                             unsigned y,
                             unsigned width,
                             unsigned height) noexcept;

    static void set_clear_color(const glm::vec4& color) noexcept;
    static void clear() noexcept;

    static void draw_indexed(const VertexArray& vertex_array) noexcept;
};

}

#endif
