#ifndef REAL_TEXTURE_HPP
#define REAL_TEXTURE_HPP

#include "core/Core.hpp"

#include <filesystem>

namespace Real {

/** Abstract base class for all textures. */
class Texture {
  public:
    virtual ~Texture() = default;

    virtual unsigned get_width() const  = 0;
    virtual unsigned get_height() const = 0;

    virtual void bind(unsigned slot = 0) const = 0;
};

/** Abstract class for all two-dimensional textures. Actual textures are
 * realized in render API specific classes. */
class Texture2D : public Texture {
  public:
    static std::shared_ptr<Texture2D> create(const std::filesystem::path& filepath);
};

}

#endif
