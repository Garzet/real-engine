#ifndef REAL_INPUT_HPP
#define REAL_INPUT_HPP

#include "core/Core.hpp"
#include "core/Key.hpp"

#include <utility>
#include <memory>

namespace Real {

/* Input abstraction for input polling. This is a base class and is a singleton.
 * Platform specific implementations of input polling are done by inheriting
 * this class and overriding protected functions. */
class Input {
  private:
    static std::unique_ptr<Input> instance;

  public:
    virtual ~Input() = default;

    static bool is_key_pressed(Key key)
    {
        return instance->is_key_pressed_impl(key);
    }

    static bool is_mouse_button_pressed(int button)
    {
        return instance->is_mouse_button_pressed_impl(button);
    }

    static float get_mouse_x() { return instance->get_mouse_x_impl(); }

    static float get_mouse_y() { return instance->get_mouse_y_impl(); }

    static std::pair<float, float> get_mouse_position()
    {
        return instance->get_mouse_position_impl();
    }

  protected:
    // Platform specific implementations of polling input.
    virtual bool is_key_pressed_impl(Key key)                 = 0;
    virtual bool is_mouse_button_pressed_impl(int button)     = 0;
    virtual float get_mouse_x_impl()                          = 0;
    virtual float get_mouse_y_impl()                          = 0;
    virtual std::pair<float, float> get_mouse_position_impl() = 0;
};

}

#endif
