#ifndef REAL_MOUSE_BUTTON_CODES_HPP
#define REAL_MOUSE_BUTTON_CODES_HPP

/* Mouse button codes taken from GLFW library. */

namespace Real {

enum class Mouse : int {
    Button1      = 0,
    Button2      = 1,
    Button3      = 2,
    Button4      = 3,
    Button5      = 4,
    Button6      = 5,
    Button7      = 6,
    Button8      = 7,
    ButtonLast   = Button8,
    ButtonLeft   = Button1,
    ButtonRight  = Button2,
    ButtonMiddle = Button3
};

}

#endif
