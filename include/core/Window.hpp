#ifndef REAL_WINDOW_HPP
#define REAL_WINDOW_HPP

#include "core/Event.hpp"

namespace Real {

struct WindowProperties {
    static constexpr auto default_title  = "Real Engine";
    static constexpr auto default_width  = 1280u;
    static constexpr auto default_height = 720u;

    std::string title;
    unsigned width;
    unsigned height;

    WindowProperties(std::string title = default_title,
                     unsigned width    = default_width,
                     unsigned height   = default_height);
};

/** Interface representing a desktop system based window. This is what
 * application uses since it is operating system agnostic. */
class Window {
  public:
    using EventCallback = std::function<void(Event&)>;

    Window()                    = default;
    virtual ~Window()           = default;
    Window(const Window& other) = delete;
    Window(Window&& other)      = delete;
    Window& operator=(const Window& other) = delete;
    Window& operator=(Window&& other) = delete;

    virtual void on_update() = 0;

    virtual unsigned get_width() const  = 0;
    virtual unsigned get_height() const = 0;

    // Window attributes.
    virtual void set_event_callback(EventCallback callback) = 0;
    virtual void set_vsync(bool enabled)                    = 0;
    virtual bool is_vsync() const                           = 0;

    /** Returns a pointer to native window object (for example, GLFW window).
     * This is exposed in the public API so that client applications can access
     * platform specific features of a window if they so desire. Extensibility
     * of the engine is improved by allowing this access. */
    virtual void* get_native_window() const = 0;

    /** Window creation function. This will be implemented for each platform
     * separately. */
    static std::unique_ptr<Window>
    create(const WindowProperties& props = WindowProperties());
};

}

#endif
