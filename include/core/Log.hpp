#ifndef REAL_LOG_HPP
#define REAL_LOG_HPP

#include <fmt/color.h>
#include <fmt/ostream.h> // TODO: Replace with formatter specializations.

#include "core/Core.hpp"

namespace Real {

class Log {
  public:
    class Core {
      public:
        template<typename... T>
        static void normal(std::string_view format_string, const T&... args);

        template<typename... T>
        static void success(std::string_view format_string, const T&... args);

        template<typename... T>
        static void warn(std::string_view format_string, const T&... args);

        template<typename... T>
        static void error(std::string_view format_string, const T&... args);

      private:
        static std::string reformat(std::string_view format_string);
    };

    template<typename... T>
    static void normal(std::string_view format_string, const T&... args);

    template<typename... T>
    static void success(std::string_view format_string, const T&... args);

    template<typename... T>
    static void warn(std::string_view format_string, const T&... args);

    template<typename... T>
    static void error(std::string_view format_string, const T&... args);

  private:
    static std::string reformat(std::string_view format_string);
};

#ifndef REAL_NLOG

template<typename... T>
void Log::Core::normal(std::string_view format_string, const T&... args)
{
    fmt::print(reformat(format_string), args...);
}

template<typename... T>
void Log::Core::success(std::string_view format_string, const T&... args)
{
    fmt::print(fg(fmt::color::green), reformat(format_string), args...);
}

template<typename... T>
void Log::Core::warn(std::string_view format_string, const T&... args)
{
    fmt::print(fg(fmt::color::yellow), reformat(format_string), args...);
}

template<typename... T>
void Log::Core::error(std::string_view format_string, const T&... args)
{
    fmt::print(stderr, fg(fmt::color::red), reformat(format_string), args...);
}

template<typename... T>
void Log::normal(std::string_view format_string, const T&... args)
{
    fmt::print(reformat(format_string), args...);
}

template<typename... T>
void Log::success(std::string_view format_string, const T&... args)
{
    fmt::print(fg(fmt::color::green), reformat(format_string), args...);
}

template<typename... T>
void Log::warn(std::string_view format_string, const T&... args)
{
    fmt::print(fg(fmt::color::yellow), reformat(format_string), args...);
}

template<typename... T>
void Log::error(std::string_view format_string, const T&... args)
{
    fmt::print(stderr, fg(fmt::color::red), reformat(format_string), args...);
}

#else

template<typename... T>
void Log::Core::normal(std::string_view /*format_string*/, const T&... /*args*/)
{}

template<typename... T>
void Log::Core::success(std::string_view /*format_string*/,
                        const T&... /*args*/)
{}

template<typename... T>
void Log::Core::warn(std::string_view /*format_string*/, const T&... /*args*/)
{}

template<typename... T>
void Log::Core::error(std::string_view /*format_string*/, const T&... /*args*/)
{}

template<typename... T>
void Log::normal(std::string_view /*format_string*/, const T&... /*args*/)
{}

template<typename... T>
void Log::success(std::string_view /*format_string*/, const T&... /*args*/)
{}

template<typename... T>
void Log::warn(std::string_view /*format_string*/, const T&... /*args*/)
{}

template<typename... T>
void Log::error(std::string_view /*format_string*/, const T&... /*args*/)
{}

#endif

}

#endif
