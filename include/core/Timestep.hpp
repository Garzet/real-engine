#ifndef REAL_TIMESTEP_HPP
#define REAL_TIMESTEP_HPP

#include <chrono>
#include <ostream>

#include "Core.hpp"

namespace Real {

/* Clock to use for measuring time. */
using TimestepClock = std::chrono::high_resolution_clock;

/* Type storing high-resolution time point. This is an probably an integral
 * type that counts number of ticks (ticks are likely nanoseconds). */
using Timepoint = TimestepClock::time_point;

/* Type storing high-resolution time. This is an integral type that counts
 * number of ticks (ticks are likely nanoseconds). */
using Duration = TimestepClock::duration;

/* Abstraction of a timestep which holds high resolution time. On some
 * platforms, this might be precise to nanoseconds. Most platforms are expected
 * to be precise to 100 nanoseconds. This class is a thin wrapper around a
 * duration object so it can be safely passed by value. */
class Timestep {
  private:
    Duration time;

  public:
    Timestep(Duration time = std::chrono::seconds(0)) noexcept;

    std::chrono::seconds get_seconds() const noexcept;
    std::chrono::milliseconds get_milliseconds() const noexcept;
    std::chrono::microseconds get_microseconds() const noexcept;
    std::chrono::nanoseconds get_nanoseconds() const noexcept;

    /* Implicit casting to float is enabled. This returns time in seconds as a
     * floating point value. */
    operator float() const noexcept;

    static Timepoint get_current_timepoint() noexcept;

    friend std::ostream& operator<<(std::ostream& s, const Timestep& timestep);
};

}

#endif
