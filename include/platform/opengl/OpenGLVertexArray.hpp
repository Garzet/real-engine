#ifndef REAL_OPENGL_VERTEX_ARRAY_HPP
#define REAL_OPENGL_VERTEX_ARRAY_HPP

#include "renderer/VertexArray.hpp"

namespace Real {

class OpenGLVertexArray : public VertexArray {
  private:
    unsigned renderer_id;
    std::vector<std::shared_ptr<VertexBuffer>> vertex_buffers;
    std::shared_ptr<IndexBuffer> index_buffer;

  public:
    OpenGLVertexArray() noexcept;
    ~OpenGLVertexArray() noexcept override;

    OpenGLVertexArray(const OpenGLVertexArray& other) = delete;
    OpenGLVertexArray& operator=(const OpenGLVertexArray& other) = delete;

    OpenGLVertexArray(OpenGLVertexArray&& other) noexcept;
    OpenGLVertexArray& operator=(OpenGLVertexArray&& other) noexcept;

    void bind() const noexcept override;
    void unbind() const noexcept override;

    void
    add_vertex_buffer(std::shared_ptr<VertexBuffer> vertex_buffer) override;

    void set_index_buffer(
        std::shared_ptr<IndexBuffer> index_buffer) noexcept override;

    const std::shared_ptr<IndexBuffer>&
    get_index_buffer() const noexcept override;

    const std::vector<std::shared_ptr<VertexBuffer>>&
    get_vertex_buffers() const noexcept override;
};

}

#endif
