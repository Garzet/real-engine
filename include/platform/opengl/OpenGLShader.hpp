#ifndef REAL_OPENGL_SHADER_HPP
#define REAL_OPENGL_SHADER_HPP

#include "renderer/Shader.hpp"

#include <glm/glm.hpp>

#include <unordered_map>
#include <filesystem>

namespace Real {

enum class ShaderType : unsigned;

class OpenGLShader : public Shader {
  private:
    /** Number that uniquely identifies the shader program object inside the
     * rendering API (for example, OpenGL). This unique identifier is sometimes
     * referred to as a handle. */
    unsigned renderer_id;

    /** Name of the shader asset. Users can use this name to reference the
     * shader so this name should be unique. */
    std::string name;

  public:
    /** Read a shader source code from the provided file, compile it and link
     * it. Shader name is set as the stem of the proivided path (file name
     * without path and extension). */
    OpenGLShader(const std::filesystem::path& filepath);

    OpenGLShader(const std::string& name,
                 const std::string& vertex_src,
                 const std::string& fragment_src);

    ~OpenGLShader() override;

    void bind() const override;
    void unbind() const override;

    const std::string& get_name() const override { return name; }

    /* Set uniform functions. These can do more than simply upload uniforms so
     * they are considered higher level. */
    void set_uniform_int(const std::string& name, int value) override;
    void set_uniform_float3(const std::string& name,
                            const glm::vec3& vec) override;
    void set_uniform_float4(const std::string& name,
                            const glm::vec4& vec) override;
    void set_uniform_mat4(const std::string& name,
                          const glm::mat4& mat) override;

    /* Set uniform functions. These can do more than simply upload uniforms so
     * they are considered higher level. */
    void upload_uniform_int(const std::string& name, int value);
    void upload_uniform_float(const std::string& name, float value);
    void upload_uniform_float2(const std::string& name, const glm::vec2& vec);
    void upload_uniform_float3(const std::string& name, const glm::vec3& vec);
    void upload_uniform_float4(const std::string& name, const glm::vec4& vec);
    void upload_uniform_mat3(const std::string& name, const glm::mat3& matrix);
    void upload_uniform_mat4(const std::string& name, const glm::mat4& matrix);

  private:
    /** Reads a file with the provided path and returns its contents. */
    std::string read_file(const std::filesystem::path& filepath);

    /** Processes provided string and creates a mapping of a shader type and
     * shader source code. When multiple shaders type are stored in a single
     * file, this function is used to split the file contents. */
    std::unordered_map<ShaderType, std::string>
    preprocess(const std::string& src);

    /** Links and validates shader program based on attached shaders. */
    void link_shader() const;
};

}

#endif
