#ifndef REAL_LINUX_INPUT_HPP
#define REAL_LINUX_INPUT_HPP

#include "core/Input.hpp"

namespace Real {

class LinuxInput : public Input {
  protected:
    bool is_key_pressed_impl(Key key) override;
    bool is_mouse_button_pressed_impl(int button) override;
    float get_mouse_x_impl() override;
    float get_mouse_y_impl() override;
    std::pair<float, float> get_mouse_position_impl() override;
};

}

#endif
