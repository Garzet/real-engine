#ifndef ENTRY_POINT_HPP
#define ENTRY_POINT_HPP

#include "core/Application.hpp"
#include "core/Log.hpp"

extern std::unique_ptr<Real::Application> Real::create_application();

int main(int /*argc*/, char** /*args*/)
{
    std::unique_ptr<Real::Application> app = Real::create_application();
    app->run();
}

#endif
